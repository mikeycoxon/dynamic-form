## [0.0.1]
- Initial project setup and base build.
- Most model layouts working, but work on all the callforwards needed to get them going.

## [0.0.2]
- Fixed various display issues
- Added a json viewer to show the criterion structure

## [0.0.3]
- updated README with more helpful documentation

## [0.0.4]
- fixed checkbox (issue #1)

## [0.0.5]
- fixed custom validation error handling (didn't realise that it was an issue)

## [0.0.6]
- improved declared display control, haven't got function calling yet.
