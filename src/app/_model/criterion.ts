import {Field} from './field';
import {Endpoint} from './endpoint';

export interface Criterion {
  page?: number;
  pages?: number;
  name?: string;
  feature?: string;
  endpoints?: Endpoint[];
  fields?: Field[];
}

export class Criterion implements Criterion {

  constructor(criterion: Criterion) {
    Object.assign(this, criterion);
  }
}

export class CriterionBuilder implements Partial<Criterion> {
  page?: number;
  pages?: number;
  name?: string;
  feature?: string;
  endpoints?: Endpoint[];
  fields?: Field[];

  withPage(value: number): this & Pick<Criterion, 'page'> {
    return Object.assign(this, { page: value });
  }

  withPages(value: number): this & Pick<Criterion, 'pages'> {
    return Object.assign(this, { pages: value });
  }

  withName(value: string): this & Pick<Criterion, 'name'> {
    return Object.assign(this, { name: value });
  }

  withFeature(value: string): this & Pick<Criterion, 'feature'> {
    return Object.assign(this, { feature: value });
  }

  addEndpoint(value: Endpoint): this & Pick<Criterion, 'endpoints'> {
    if (this.endpoints) {
      this.endpoints.push(value);
      return this;
    }
    return Object.assign(this, {endpoints: [value]});
  }

  addField(value: Field): this & Pick<Criterion, 'fields'> {
    if (this.fields) {
      this.fields.push(value);
      return this;
    }
    return Object.assign(this, {fields: [value]});
  }

  build(this: Criterion) {
    return new Criterion(this);
  }


}
