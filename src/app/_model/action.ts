import {Contingency} from './contingency';

export interface Action {
  when?: Contingency;
  call?: string;
}

export class Action implements Action {
  constructor(action: Action) {
    Object.assign(this, action);
  }
}

export class ActionBuilder implements Partial<Action> {
  when?: Contingency;
  call?: string;

  withWhen(value: Contingency): this & Pick<Action, 'when'> {
    return Object.assign(this, { when: value });
  }

  withCall(value: string): this & Pick<Action, 'call'> {
    return Object.assign(this, { call: value });
  }

  build(this: Action) {
    return new Action(this);
  }

}
