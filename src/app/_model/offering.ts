import {Criterion} from './criterion';

export class Offering {
  constructor(
    name?: string,
    criteria?: Criterion[]
  ) {}
}
