import {EndpointMethod} from './enums';
import {PrettyMap} from '@largesnike/pretty-map/lib/pretty.map';

export interface Endpoint {
  name?: string;
  method?: EndpointMethod;
  params?: PrettyMap<string, string>;
  headers?: PrettyMap<string, string>;

}

export class Endpoint implements Endpoint {

  constructor(endpoint: Endpoint) {
    Object.assign(this, endpoint);
  }
}

export class EndpointBuilder implements Partial<Endpoint> {
  name?: string;
  method?: EndpointMethod;
  params?: PrettyMap<string, string>;
  headers?: PrettyMap<string, string>;

  withName(value: string): this & Pick<Endpoint, 'name'> {
    return Object.assign(this, { name: value });
  }

  withMethod(value: EndpointMethod): this & Pick<Endpoint, 'method'> {
    return Object.assign(this, { method: value });
  }

  addParam(name: string, value: string): this & Pick<Endpoint, 'params'> {
    if (this.params) {
      this.params.set(name, value);
      return this;
    }
    return Object.assign(this, {params: new PrettyMap([[name, value]])});
  }

  addHeader(name: string, value: string): this & Pick<Endpoint, 'headers'> {
    if (this.headers) {
      this.headers.set(name, value);
      return this;
    }
    return Object.assign(this, {headers: new PrettyMap([[name, value]])});
  }

  build(this: Endpoint) {
    return new Endpoint(this);
  }


}
