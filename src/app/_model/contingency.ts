import {FieldCondition} from './enums';

export interface Contingency {
  fieldOfName?: string;
  condition?: FieldCondition;
}

export class Contingency implements Contingency {
  constructor(action: Contingency) {
    Object.assign(this, action);
  }
}

export class ContingencyBuilder implements Partial<Contingency> {
  fieldOfName?: string;
  condition?: FieldCondition;

  withFieldOfName(value: string): this & Pick<Contingency, 'fieldOfName'> {
    return Object.assign(this, { fieldOfName: value });
  }

  withCondition(value: FieldCondition): this & Pick<Contingency, 'condition'> {
    return Object.assign(this, { condition: value });
  }

  build(this: Contingency) {
    return new Contingency(this);
  }
}
