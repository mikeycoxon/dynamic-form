import {FieldType} from './enums';
import {Validation} from './validation';
import {Action} from './action';
import {Contingency} from './contingency';


export interface Field {
  /**
   * name     This is both the name of the field in terms of the form and a tag for the display logic: it provides a way to select the
   * display for its value
   */
  name?: string;

  /**
   * val    The value of the display. This is usually populated when the user enters something, but it may be pre-populated
   *          in the case of placeholder text in text fields, a selected value in checkboxes, or the src value in an iframe
   *          or hyperlink, or even body text on the fields.
   */
  val?: string;
  /**
   * display    Referring to the display itself, and referring to its type; i.e. whether it is a text display, an anchor
   *          a checkbox, radio button, iframe, password display, or simple free text on the panel.
   */
  display?: FieldType;

  /**
   * hide     When this is true the display starts off as visibility on the form. In most cases, when this is true
   *          the following shown member is not null and has a {@link Contingency} attached to it.
   */
  hide?: boolean;

  /**
   * hideShow A setting on the last member allowing it to become visible (if hidden) based on a contingency.
   */
  hideShow?: Contingency;
  label?: string;
  validations?: Validation[];
  action?: Action;

  /**
   * Normally declared on its own in a field to assist laying out the elements: it simply forces a new line in the form layout.
   */
  newLine?: boolean;

}
export class Field implements Field {

  constructor(field: Field) {
    Object.assign(this, field);
  }

}

export class FieldBuilder implements Partial<Field> {
  name?: string;
  val?: string;
  display?: FieldType;
  hide?: boolean;
  hideShow?: Contingency;
  label?: string;
  validations?: Validation[];
  action?: Action;
  newLine = false;

  withName(value: string): this & Pick<Field, 'name'> {
    return Object.assign(this, { name: value });
  }

  withValue(value: string): this & Pick<Field, 'val'> {
    return Object.assign(this, { val: value });
  }

  withDisplay(value: FieldType): this & Pick<Field, 'display'> {
    return Object.assign(this, { display: value });
  }

  withHide(value: boolean): this & Pick<Field, 'hide'> {
    return Object.assign(this, { hide: value });
  }

  withHideShow(value: Contingency): this & Pick<Field, 'hideShow'> {
    return Object.assign(this, { hideShow: value });
  }

  withLabel(value: string): this & Pick<Field, 'label'> {
    return Object.assign(this, { label: value });
  }

  addValidation(value: Validation): this & Pick<Field, 'validations'> {
    if (this.validations) {
      this.validations.push(value);
      return this;
    }
    return Object.assign(this, {validations: [value]});
  }

  withAction(value: Action): this & Pick<Field, 'action'> {
    return Object.assign(this, { action: value });
  }

  withNewLine(): this & Pick<Field, 'newLine'> {
    return Object.assign(this, { newLine: true });
  }

  build(this: Field) {
    return new Field(this);
  }


}
