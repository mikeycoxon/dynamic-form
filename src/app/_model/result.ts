import {ResponseCondition} from './enums';

export interface Result {
  endpoint?: string;
  condition?: ResponseCondition;
}

export class Result implements Result {
  constructor(result: Result) {
    Object.assign(this, result);
  }
}

export class ResultBuilder implements Partial<Result> {
  endpoint?: string;
  condition?: ResponseCondition;

  withEndpointOfName(value: string): this & Pick<Result, 'endpoint'> {
    return Object.assign(this, { endpoint: value });
  }

  withCondition(value: ResponseCondition): this & Pick<Result, 'condition'> {
    return Object.assign(this, { condition: value });
  }

  build(this: Result) {
    return new Result(this);
  }

}
