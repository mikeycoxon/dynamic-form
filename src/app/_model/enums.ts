import {PrettyMap} from '@largesnike/pretty-map/lib/pretty.map';

export enum FieldType {
  Text = 'text',
  Checkbox = 'checkbox',
  IFrame = 'iframe',
  Password = 'password',
  Anchor = 'anchor',
  BodyText = 'bodytext',
  Button = 'button'
}

export enum NeedsValid {
  Numeric = 'numeric',          // numbers only
  Alpha = 'alpha',              // mixed case letters only
  Uppercase = 'uppercase',      // upper case letters only
  Lowercase = 'lowercase',      // lower case letters only
  Name = 'name',           // allows mixed case, space, apostrophe and hyphen
  AddressLine = 'addressLine',  // allows mixed case, space, apostophe, numeric, hyphen, and period
  Email = 'email',         // case insensitive, each at symbol, and dots with valid domain name
  ABN = 'ABN',             // 11 digit number conforming to a checksum
  Postcode = 'postcode',
  Phone = 'auPhone',
  Mobile = 'auMobile',
  Landline = 'auLandline'
}

export enum FieldCondition {
  isValid = 'valid'
}

export enum ResponseCondition {
  is2xx = 'is2xx',
  is4xx = 'is4xx',
  is5xx = 'is5xx'
}

export enum EndpointMethod {
  Get = 'get',
  Post = 'post',
  Put = 'put',
  Delete = 'delete'
}

export function toArray(theEnum) {
  return Object.keys(theEnum)
    .slice(0, Object.keys(theEnum).length)
    .map(k => theEnum[k]);
}

export function toMap(theEnum) {
  const pm = new PrettyMap<string, string>([]);
  Object.keys(theEnum)
      .forEach(n => {
        if (typeof theEnum[n] === 'string') {
          pm.set(n, theEnum[n] as string);
        }
      });
  return pm;
}

