import {NeedsValid} from './enums';
import {Result} from './result';

export interface Validation {
  required?: boolean;
  minLength?: number;
  maxLength?: number;
  customNeeds?: NeedsValid[];
  result?: Result;
  errMsg?: string;
}

export class Validation implements Validation {
  constructor(validation: Validation) {
    Object.assign(this, validation);
  }
}

export class ValidationBuilder implements Partial<Validation> {
  required?: boolean;
  minLength?: number;
  maxLength?: number;
  customNeeds?: NeedsValid[];
  result?: Result;
  errMsg?: string;

  isRequired(): this & Pick<Validation, 'required'> {
    return Object.assign(this, { required: true });
  }

  withMinLength(value: number): this & Pick<Validation, 'minLength'> {
    return Object.assign(this, { minLength: value });
  }

  withMaxLength(value: number): this & Pick<Validation, 'maxLength'> {
    return Object.assign(this, { maxLength: value });
  }

  addSupportedChars(value: NeedsValid): this & Pick<Validation, 'customNeeds'> {
    if (this.customNeeds) {
      this.customNeeds.push(value);
      return this;
    }
    return Object.assign(this, {customNeeds: [value]});
  }


  withResult(value: Result): this & Pick<Validation, 'result'> {
    return Object.assign(this, { result: value });
  }

  withErrMsg(value: string): this & Pick<Validation, 'errMsg'> {
    return Object.assign(this, { errMsg: value });
  }

  build(this: Validation) {
    return new Validation(this);
  }

}

