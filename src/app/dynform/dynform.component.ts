import {Component, OnInit, Renderer2} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {CriteriaService} from '../_services/criteria.service';
import {Criterion} from '../_model/criterion';
import {FieldCondition, FieldType, NeedsValid, toMap} from '../_model/enums';
import {PrettyMap} from '@largesnike/pretty-map/lib/pretty.map';
import {Field} from '../_model/field';
import {
  hasValidAbn,
  hasValidAuLandline,
  hasValidAuMobile,
  hasValidAuPhone,
  hasValidEmail,
  hasValidName
} from '../_helpers/custom.validators';
import {CallOutService} from '../_services/call.out.service';
import {Action} from '../_model/action';

@Component({
  selector: 'app-dynform',
  templateUrl: './dynform.component.html',
  styleUrls: ['./dynform.component.scss']
})
export class DynformComponent implements OnInit {

  selectForm: FormGroup;
  selections: string[] = [
    'ask-2-names',
    'ask-4-names',
    'ask-address',
    'ask-abn',
    'ask-verify-mobile',
    'ask-terms'
  ];

  dynForm: FormGroup;
  criterion: Criterion;
  fieldTypes: PrettyMap<string, string>;

  constructor(
    private fb: FormBuilder,
    private api: CriteriaService,
    private callOut: CallOutService
  ) { }

  ngOnInit() {
    this.buildSelectForm();
    this.fieldTypes = toMap(FieldType);
    console.log(this.fieldTypes);
  }

  doGetCriterionScreen($event: any) {
    if ($event) {
      this.doGetCriterionFromApi($event.toString()).then(
        criterion => {
          this.buildDynamicForm(criterion);
        }
      );
    }
  }

  back() {
    console.log('going back, well, not really');
  }

  next() {
    console.log('going forward, well, not really');
  }


  caller(action: Action, callOut: CallOutService) {
    console.log(action);
    const fn = (control: AbstractControl) => {
      console.log(action.when.condition);
      return control[action.when.condition] ? callOut.get(action.call) : null;
    };

    return fn;
  }

  private async doGetCriterionFromApi(resource: string) {
    return await this.api.get(resource).toPromise();
  }

  buildSelectForm() {
    this.selectForm = this.fb.group({
      selection: [this.selections]
    });
  }

  buildDynamicForm(criterion: Criterion) {
    this.criterion = criterion;
    const group: FormGroup = this.fb.group({});

    this.criterion.fields.forEach(f => {
      const validatorFns: ValidatorFn[] = [];
      if (f.validations && f.validations.length > 0) {
        f.validations.forEach(v => {
          if (Object.keys(v).includes('required')) {
            validatorFns.push(Validators.required);
          } else if (Object.keys(v).includes('minLength')) {
            validatorFns.push(Validators.minLength(v.minLength));
          } else if (Object.keys(v).includes('maxLength')) {
            validatorFns.push(Validators.maxLength(v.maxLength));
          } else if (Object.keys(v).includes('customNeeds')) {
            v.customNeeds.forEach(sc => {
              if (sc === NeedsValid.Numeric) {
                validatorFns.push(Validators.pattern(/[0-9]*/));
              } else if (sc === NeedsValid.Phone) {
                validatorFns.push(hasValidAuPhone);
              } else if (sc === NeedsValid.Mobile) {
                validatorFns.push(hasValidAuMobile);
              } else if (sc === NeedsValid.Landline) {
                validatorFns.push(hasValidAuLandline);
              } else if (sc === NeedsValid.ABN) {
                validatorFns.push(hasValidAbn);
              } else if (sc === NeedsValid.Lowercase) {
                validatorFns.push(Validators.pattern(/[a-z]*/));
              } else if (sc === NeedsValid.Uppercase) {
                validatorFns.push(Validators.pattern(/[A-Z]*/));
              } else if (sc === NeedsValid.Name) {
                validatorFns.push(hasValidName);
              } else if (sc === NeedsValid.AddressLine) {
                validatorFns.push(Validators.pattern(/^[a-zA-Z0-9\s.\-]+$/));
              } else if (sc === NeedsValid.Alpha) {
                validatorFns.push(Validators.pattern(/^[a-zA-Z\s.\-]+$/));
              } else if (sc === NeedsValid.Email) {
                validatorFns.push(hasValidEmail);
              } else if (sc === NeedsValid.Postcode) {
                validatorFns.push(Validators.pattern(/[0-9]{4}/));
              }
           });
          }
        });
      }

      if (f.display === FieldType.Text ||
        f.display === FieldType.Checkbox ||
        f.display === FieldType.Password ||
        f.display === FieldType.Button) {

        let control: FormControl;
        if (validatorFns && validatorFns.length > 0) {
          console.log(validatorFns);
          control = new FormControl(f.val, validatorFns);
        } else {
          control = new FormControl(f.val);
        }

        group.addControl(f.name, control);
      }
    });

    this.dynForm = group;
    console.log(group);
  }

  errorMessageResolver(field: Field, errors: ValidationErrors) {
    if (errors.hasOwnProperty('required')) {
      return field.validations.find(v => v.required).errMsg;
    }
    if (errors.hasOwnProperty('minlength')) {
      return field.validations.find(v => v.minLength >= 0).errMsg;
    }
    if (errors.hasOwnProperty('maxlength')) {
      return field.validations.find(v => v.maxLength >= 0).errMsg;
    }
    if (errors.hasOwnProperty('needsValidABN')) {
      return field.validations.filter(v => v.customNeeds && v.customNeeds.find(cn => cn === NeedsValid.ABN)).pop().errMsg;
    }
    if (errors.hasOwnProperty('needsValidLandline')) {
      return field.validations.filter(v => v.customNeeds && v.customNeeds.find(cn => cn === NeedsValid.Landline)).pop().errMsg;
    }
    if (errors.hasOwnProperty('needsValidMobile')) {
      return field.validations.filter(v => v.customNeeds && v.customNeeds.find(cn => cn === NeedsValid.Mobile)).pop().errMsg;
    }
    if (errors.hasOwnProperty('needsValidPhone')) {
      return field.validations.filter(v => v.customNeeds && v.customNeeds.find(cn => cn === NeedsValid.Phone)).pop().errMsg;
    }
    if (errors.hasOwnProperty('needsValidEmail')) {
      return field.validations.filter(v => v.customNeeds && v.customNeeds.find(cn => cn === NeedsValid.Email)).pop().errMsg;
    }
    if (errors.hasOwnProperty('needsValidName')) {
      return field.validations.filter(v => v.customNeeds && v.customNeeds.find(cn => cn === NeedsValid.Name)).pop().errMsg;
    }
  }

}
