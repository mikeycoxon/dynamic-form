import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynformComponent } from './dynform.component';

describe('DynformComponent', () => {
  let component: DynformComponent;
  let fixture: ComponentFixture<DynformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
