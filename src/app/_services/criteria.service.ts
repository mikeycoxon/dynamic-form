import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Criterion} from '../_model/criterion';

@Injectable({
  providedIn: 'root'
})
export class CriteriaService {

  constructor(private http: HttpClient) { }

  get(resource: string) {
    console.log('get() called with ' + resource);
    return this.http.get<Criterion>('localhost:4200/api/' + resource);
  }
}
