import { TestBed } from '@angular/core/testing';

import { CallOutService } from './call.out.service';

describe('CallOutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CallOutService = TestBed.get(CallOutService);
    expect(service).toBeTruthy();
  });
});
