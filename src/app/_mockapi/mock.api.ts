import {
  HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {of, Observable, throwError} from 'rxjs';
import {Offering} from '../_model/offering';
import {delay, dematerialize, materialize, mergeMap} from 'rxjs/operators';
import {fxAbn,
  fxCriterion2Names,
  fxCriterion4Names,
  fxCriterionAddress,
  fxTerms,
  fxVerifyMobile} from '../../../test/fixtures/fx.criterion';

@Injectable()
export class MockApi implements HttpInterceptor {

  constructor() {
    localStorage.setItem('ask-2-names', JSON.stringify(fxCriterion2Names));
    localStorage.setItem('ask-4-names', JSON.stringify(fxCriterion4Names));
    localStorage.setItem('ask-address', JSON.stringify(fxCriterionAddress));
    localStorage.setItem('ask-abn', JSON.stringify(fxAbn));
    localStorage.setItem('ask-verify-mobile', JSON.stringify(fxVerifyMobile));
    localStorage.setItem('ask-terms', JSON.stringify(fxTerms));
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('intercepted: ' + request.url);
    return of(null).pipe(mergeMap(() => {

      if (request.url.endsWith('/api/ask-2-names') && request.method === 'GET') {
        const offering: Offering = JSON.parse(localStorage.getItem('ask-2-names'));
        return of(new HttpResponse({status: 200, body: offering}));
      }

      if (request.url.endsWith('/api/ask-4-names') && request.method === 'GET') {
        const offering: Offering = JSON.parse(localStorage.getItem('ask-4-names'));
        return of(new HttpResponse({status: 200, body: offering}));
      }

      if (request.url.endsWith('/api/ask-address') && request.method === 'GET') {
        const offering: Offering = JSON.parse(localStorage.getItem('ask-address'));
        return of(new HttpResponse({status: 200, body: offering}));
      }

      if (request.url.endsWith('/api/ask-abn') && request.method === 'GET') {
        const offering: Offering = JSON.parse(localStorage.getItem('ask-abn'));
        return of(new HttpResponse({status: 200, body: offering}));
      }

      if (request.url.endsWith('/api/ask-verify-mobile') && request.method === 'GET') {
        const offering: Offering = JSON.parse(localStorage.getItem('ask-verify-mobile'));
        return of(new HttpResponse({status: 200, body: offering}));
      }

      if (request.url.endsWith('/verificationcode/ok/sms') && request.method === 'POST') {
        return of(new HttpResponse({status: 200, body: null}));
      }

      if (request.url.endsWith('/verificationcode/ok/verify') && request.method === 'POST') {
        return of(new HttpResponse({status: 200, body: null}));
      }

      if (request.url.endsWith('/verificationcode/bad/sms') && request.method === 'POST') {
        return of(new HttpResponse({status: 400, body: null}));
      }

      if (request.url.endsWith('/verificationcode/bad/verify') && request.method === 'POST') {
        return of(new HttpResponse({status: 400, body: null}));
      }

      if (request.url.endsWith('/api/ask-terms') && request.method === 'GET') {
        const offering: Offering = JSON.parse(localStorage.getItem('ask-terms'));
        return of(new HttpResponse({status: 200, body: offering}));
      }

      // pass through any requests not handled above
      return next.handle(request);
    }))


      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());

    function error(message) {
      return throwError({ status: 400, error: { message } });
    }

  }
}

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: MockApi,
  multi: true
};

