import {AbstractControl} from '@angular/forms';
import {Action} from '../_model/action';
import {CallOutService} from '../_services/call.out.service';

export function caller(action: Action, callOut: CallOutService) {

  console.log(action);
  const fn = (control: AbstractControl) => {
    console.log(action.when.condition);
    return control[action.when.condition] ? callOut.get(action.call) : null;
  };

  return fn;
}
