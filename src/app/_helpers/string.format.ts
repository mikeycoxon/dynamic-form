export const isValidAbn = (candidate: string) => {

  if (!candidate || isNaN(+candidate) || candidate.length !== 11) {
    return false;
  }

  const weights: number[] = [10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19];

  const sum: number = [...Array.from({length: 11}, (v, i) => i)].map(i => {
    return weights[i] * (+candidate.charAt(i) - ((i === 0) ? 1 : 0));
  }).reduce((a, c) => a + c);

  return (sum % 89 === 0);
};


export const isValidAusPhoneNumber = (candidate: string) => {
  if (!candidate) {
    return false;
  }
  return (candidate.match(/(^1300\d{6}$)|(^1800|1900|1902\d{6}$)|(^0[2|3|7|8]{1}[0-9]{8}$)|(^13\d{4}$)|(^04\d{2,3}\d{6}$)/));
};

export const isValidAusLandlineNumber = (candidate: string) => {
  if (!candidate) {
    return false;
  }
  return (candidate.match(/^\({0,1}0(2|3|7|8)\){0,1}(\ |-){0,1}[0-9]{4}(\ |-){0,1}[0-9]{4}$/));
};

export const isValidAusMobileNumber = (candidate: string) => {
  if (!candidate) {
    return false;
  }
  return (candidate.match(/^04(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{3}(\ |-){0,1}[0-9]{3}$/));
};

export const isValidEmailAddress = (candidate: string) => {
  if (!candidate) {
    return false;
  }
  return (candidate.match(/^([\w\d\-\.]+)@{1}(([\w\d\-]{1,67})|([\w\d\-]+\.[\w\d\-]{1,67}))\.(([a-zA-Z\d]{2,4})(\.[a-zA-Z\d]{2})?)$/));
};

export const isValidName = (candidate: string) => {
  if (!candidate) {
    return false;
  }
  return (candidate.match(/^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*$/));
};
