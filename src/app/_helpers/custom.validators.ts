import {AbstractControl} from '@angular/forms';
import {
  isValidAbn,
  isValidAusLandlineNumber,
  isValidAusMobileNumber,
  isValidAusPhoneNumber,
  isValidEmailAddress,
  isValidName
} from './string.format';

export function hasValidAbn(control: AbstractControl) {
  const isValid = isValidAbn(control.value);
  return isValid ? null : { needsValidABN: true };
}

export function hasValidAuLandline(control: AbstractControl) {
  const isValid = isValidAusLandlineNumber(control.value);
  return isValid ? null : { needsValidLandline: true };
}

export function hasValidAuMobile(control: AbstractControl) {
  const isValid = isValidAusMobileNumber(control.value);
  return isValid ? null : { needsValidMobile: true };
}

export function hasValidAuPhone(control: AbstractControl) {
  const isValid = isValidAusPhoneNumber(control.value);
  return isValid ? null : { needsValidPhone: true };
}

export function hasValidEmail(control: AbstractControl) {
  const isValid = isValidEmailAddress(control.value);
  return isValid ? null : { needsValidEmail: true };
}

export function hasValidName(control: AbstractControl) {
  const isValid = isValidName(control.value);
  return isValid ? null : { needsValidName: true };
}

