import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgSelectModule } from '@ng-select/ng-select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { DynformComponent } from './dynform/dynform.component';
import {HttpClientModule} from '@angular/common/http';
import {fakeBackendProvider} from './_mockapi/mock.api';
import {ObjToKeysPipe} from './_helpers/obj.to.keys.pipe';
import {JsonTree} from 'ng2-json-view';

@NgModule({
  declarations: [
    AppComponent,
    DynformComponent,
    ObjToKeysPipe,
    JsonTree
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [fakeBackendProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
