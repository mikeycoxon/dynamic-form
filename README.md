# DynamicForm

A simple demonstration showing how Reactive Forms can be used to iterate through a JSON model and generate a form dynamically

The purpose of the demo is to show that its possible to dynamically generate a view template, based on modelled information,
rather than hard-coding a page.

The case we were trying to address was the need of the Eligibility Module to show different forms to the user based on what Offering and Criterion
they needed to qualify for, before using a product or service.

Please refer to [the documentation on Offerings](https://australiapost.jira.com/wiki/spaces/MP/pages/607456445/What+is+an+Offering) to
understand the reasoning behind the contents of the `_model` package.

Or more generally, refer to the [Eligibility RoadMap](https://australiapost.jira.com/wiki/spaces/MP/pages/551190755/Eligibility+RoadMap) to understond the 
overall context of this work.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.4.

## Dependencies of Note
The main eyebrow raiser is the use of my own Map type [pretty-map](https://www.npmjs.com/package/@largesnike/pretty-map?activeTab=versions). I did this 
because I could find no satisfactory TypeScript equivalent of the Map in ES6. There are some implementations, most notably [ts-map](https://www.npmjs.com/package/ts-map)
but they all suffer from one problem or another that I just couldn't cope with. Obviously, going beyond the prototype, you may want to consider an alternative
or continue using pretty-map if you think its good enough.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
