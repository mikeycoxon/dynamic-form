import {TestBed} from '@angular/core/testing';
import {fxAbn, fxCriterion4Names, fxCriterionAddress, fxTerms, fxVerifyMobile} from '../fixtures/fx.criterion';
import {Criterion} from '../../src/app/_model/criterion';

describe('Criterion', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    const model: Criterion = fxCriterion4Names;
    expect(model).toBeTruthy();
    console.log(JSON.stringify(model));
  });

  it('should be created', () => {
    const model: Criterion = fxVerifyMobile;
    expect(model).toBeTruthy();
    console.log(JSON.stringify(model));
  });

  it('should be created', () => {
    const model: Criterion = fxAbn;
    expect(model).toBeTruthy();
    console.log(JSON.stringify(model));
  });

  it('should be created', () => {
    const model: Criterion = fxCriterionAddress;
    expect(model).toBeTruthy();
    console.log(JSON.stringify(model));
  });

  it('should be created', () => {
    const model: Criterion = fxTerms;
    expect(model).toBeTruthy();
    console.log(JSON.stringify(model));
  });

});
