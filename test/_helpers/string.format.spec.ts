import {TestBed} from '@angular/core/testing';
import {
  isValidAbn,
  isValidAusLandlineNumber,
  isValidAusMobileNumber,
  isValidAusPhoneNumber,
  isValidEmailAddress, isValidName
} from '../../src/app/_helpers/string.format';

describe('WorkOrderPageModel', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should find these to be valid ABNs', () => {
    let isValid = isValidAbn('11223491505');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004044937');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004044969');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004045391');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004045440');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004045472');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004045521');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004045553');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004045585');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004045602');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('12004045634');
    expect(isValid).toBeTruthy();

    isValid = isValidAbn('11111111111');
    expect(isValid).toBeFalsy();

  });

  it('should find these to be valid Australian Phone Numbers', () => {
    let isValid = isValidAusPhoneNumber('0732105432');
    expect(isValid).toBeTruthy();

    isValid = isValidAusPhoneNumber('1300333444');
    expect(isValid).toBeTruthy();

    isValid = isValidAusPhoneNumber('131313');
    expect(isValid).toBeTruthy();

    isValid = isValidAusPhoneNumber('32105432');
    expect(isValid).toBeFalsy();

    isValid = isValidAusPhoneNumber('13000456');
    expect(isValid).toBeFalsy();
  });

  it('should find these to be valid Australian landline numbers', () => {
    let isValid = isValidAusLandlineNumber('(02) 9323 1234');
    expect(isValid).toBeTruthy();

    isValid = isValidAusLandlineNumber('0293231234');
    expect(isValid).toBeTruthy();

    isValid = isValidAusLandlineNumber('02-9323-1234');
    expect(isValid).toBeTruthy();

    isValid = isValidAusLandlineNumber('01 9323 1234');
    expect(isValid).toBeFalsy();

    isValid = isValidAusLandlineNumber('02 932 123');
    expect(isValid).toBeFalsy();

    isValid = isValidAusLandlineNumber('02/9323/1234');
    expect(isValid).toBeFalsy();
  });

  it('should find these to be valid Australian mobile numbers', () => {
    let isValid = isValidAusMobileNumber('0415118180');
    expect(isValid).toBeTruthy();

    isValid = isValidAusMobileNumber('0415 118 180');
    expect(isValid).toBeTruthy();

    isValid = isValidAusMobileNumber('0402 123 456');
    expect(isValid).toBeTruthy();

    isValid = isValidAusMobileNumber('0301222333');
    expect(isValid).toBeFalsy();

    isValid = isValidAusMobileNumber('0415 118 18');
    expect(isValid).toBeFalsy();

    isValid = isValidAusMobileNumber('0415 118 1822');
    expect(isValid).toBeFalsy();
  });

  it('should find these to be valid email addresses', () => {
    let isValid = isValidEmailAddress('foo@foo.com');
    expect(isValid).toBeTruthy();

    isValid = isValidEmailAddress('foo@foo-foo.com.au');
    expect(isValid).toBeTruthy();

    isValid = isValidEmailAddress('foo@foo.foo.info');
    expect(isValid).toBeTruthy();

    isValid = isValidEmailAddress('Michael.Coxon@auspost.com.au');
    expect(isValid).toBeTruthy();

    isValid = isValidEmailAddress('foo@.com');
    expect(isValid).toBeFalsy();

    isValid = isValidEmailAddress('foo@foo..com');
    expect(isValid).toBeFalsy();

    isValid = isValidEmailAddress('foo@me@.com');
    expect(isValid).toBeFalsy();

    isValid = isValidEmailAddress('Catherine.O\'Brien@auspost.com.au');
    expect(isValid).toBeFalsy();
  });

  it('should find these to be valid first, last or middle names', () => {
    let isValid = isValidName('T.F. Johnson');
    expect(isValid).toBeTruthy();

    isValid = isValidName('John O\'Neil');
    expect(isValid).toBeTruthy();

    isValid = isValidName('Mary-Kate Johnson');
    expect(isValid).toBeTruthy();

    isValid = isValidName('deForrest Kelly');
    expect(isValid).toBeTruthy();

    isValid = isValidName('sam_johnson');
    expect(isValid).toBeFalsy();

    isValid = isValidName('Joe--Bob Jones');
    expect(isValid).toBeFalsy();

    isValid = isValidName('dfjsd0rd');
    expect(isValid).toBeFalsy();
  });

});
